using System.Collections.Generic;
using UnityEngine;

public class HealthIndicator : MonoBehaviour
{
    // Props
    public List<Sprite> _states;

    // Members
    Game _game;
    Animator _animator;
    SpriteRenderer _renderer;

    int _currentState = -1;
    int _nextState = 0;

    private void Awake()
    {
        _game = GameObject.FindWithTag("GameController").GetComponent<Game>();
        _animator = GetComponent<Animator>();
        _renderer = transform.Find("Sprite").GetComponent<SpriteRenderer>();

        Game.OnHealthChanged += OnHealthChanged;
    }

    private void OnDestroy()
    {
        Game.OnHealthChanged -= OnHealthChanged;
    }

    private void Start()
    {
        // Initialize sprite state
        int index = (int)(_game.Health / _game.MaxHealth * _states.Count);
        index = Mathf.Clamp(index, 0, _states.Count - 1);
        _renderer.sprite = _states[index];
    }

    private void OnHealthChanged(float health)
    {
        int index = (int)(health / _game.MaxHealth * _states.Count);
        index = Mathf.Clamp(index, 0, _states.Count - 1);
    
        if (index != _currentState)
        {
            _nextState = index;
            _animator.SetTrigger("HealthChanged");
        }
    }

    private void UpdateState()
    {
        _currentState = _nextState;
        _renderer.sprite = _states[_currentState];
    }
}
