using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;

public class CurveGenerator : MonoBehaviour
{
    // Props
    public Transform _curvePrefab;
    public Transform _beatNumberPrefab;

    public float _xUnitsPerBeat = 3;
    public float _yMin = -5;
    public float _yMax = 5;

    public bool _displayBeatNumbers = false;

    // Members
    private MusicPlayer _musicPlayer;
    private List<List<Step>> _steps;
    private List<LineRenderer> _lines;
    private List<Transform> _beatNumbers;

    void Awake()
    {
        _musicPlayer = GameObject.FindWithTag("MusicPlayer").GetComponent<MusicPlayer>();

        _lines = new List<LineRenderer>();
        _beatNumbers = new List<Transform>();

        MusicPlayer.OnSongLoaded += OnSongLoaded;
        MusicPlayer.OnSongStopped += OnSongStopped;
    }

    void OnDestroy()
    {
        MusicPlayer.OnSongLoaded -= OnSongLoaded;
        MusicPlayer.OnSongStopped -= OnSongStopped;
    }

    private void OnSongLoaded()
    {
        Step[] steps = _musicPlayer.Steps;

        // Chop into individual lines with every time pitch drops to -1
        _steps = new List<List<Step>>();
        List<Step> currentLine = new List<Step>();
        _steps.Add(currentLine);
        foreach (Step step in steps)
        {
            if (step.pitch == -1)
            {
                currentLine = new List<Step>();
                _steps.Add(currentLine);    
            }
            else
            {
                currentLine.Add(step);
            }
        }

        // Remove last line if it's empty (narrator: it should be)
        if (!_steps[_steps.Count - 1].Any())
        {
            _steps.RemoveAt(_steps.Count - 1);
        }

        // TODO: Generate those only as we need them for performance, maybe?
        foreach(List<Step> step in _steps)
        {
            _lines.Add(GenerateLine(step));
        }

        if (_displayBeatNumbers)
        {
            GenerateBeatNumbers();
        }
    }

    private void OnSongStopped()
    {
        foreach (LineRenderer line in _lines)
        {
            GameObject.Destroy(line.gameObject);
        }
        _lines.Clear();

        foreach (Transform beatNumber in _beatNumbers)
        {
            GameObject.Destroy(beatNumber.gameObject);
        }
        _beatNumbers.Clear();
    }

    private LineRenderer GenerateLine(List<Step> steps)
    {
        Transform lineObj = Instantiate(_curvePrefab, transform.position, transform.rotation);
        lineObj.SetParent(transform);

        LineRenderer line = lineObj.GetComponent<LineRenderer>();
        Vector3[] positions = new Vector3[steps.Count];
        foreach (var (step, i) in steps.Select((value, i) => (value, i)))
        {
            positions[i] = new Vector3(_xUnitsPerBeat * step.beat, _yMin + (_yMax - _yMin) * step.pitch, 0f);
        }

        line.positionCount = positions.Length;
        line.SetPositions(positions);

        // Curvehead
        Transform curvehead = lineObj.Find("Curvehead");
        curvehead.localPosition = positions[0];

        return line;
    }

    private void GenerateBeatNumbers()
    {
        int numberOfBeats = (int)(_musicPlayer.BPM / 60f * _musicPlayer.SongLength);

        for (int i = 4; i < numberOfBeats; i+=4)
        {
            Transform beatNumber = Instantiate(_beatNumberPrefab, transform.position + new Vector3(_xUnitsPerBeat * i, 0f, 0f), transform.rotation);
            beatNumber.SetParent(transform);

            Text textfield = beatNumber.GetComponentInChildren<Text>();
            textfield.text = i.ToString();

            _beatNumbers.Add(beatNumber);
        }
    }
}
