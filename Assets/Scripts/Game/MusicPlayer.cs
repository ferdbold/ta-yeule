using System.IO;
using UnityEditor;
using UnityEngine;

public class MusicPlayer : MonoBehaviour
{
    public const float BaseVolume = 0.1f;

    // Components
    private AudioSource _audioSource;
    
    // Events
    public delegate void SongAction();
    public static event SongAction OnSongLoaded;
    public static event SongAction OnSongStarted;
    public static event SongAction OnSongPaused;
    public static event SongAction OnSongStopped;

    // Members
    private Stepchart _stepchart;

    void Awake()
    {
        _audioSource = GetComponent<AudioSource>();
    }

    public void Load(AudioClip song)
    {
        _audioSource.clip = song;
        _stepchart = LoadStepchart(song);

        OnSongLoaded();
    }

    public void Play()
    {
        _audioSource.Play();
        
        OnSongStarted();
    }

    public void Pause()
    {
        _audioSource.Pause();

        OnSongPaused();
    }

    public void Stop()
    {
        _audioSource.Stop();
        _audioSource.clip = null;
        
        OnSongStopped();
    }

    public void LowerVoice( float amount )
    {
        _audioSource.volume = _audioSource.volume + amount;
    }

    public void RefreshVoice()
    {
        _audioSource.volume = BaseVolume;
    }

    private Stepchart LoadStepchart(AudioClip song) 
    {
        // Get file name
        string path = AssetDatabase.GetAssetPath(song);
        string[] pathSplit = path.Split('/');
        string fileName = pathSplit[pathSplit.Length - 1];
        string[] fileNameSplit = fileName.Split('.');
        fileName = fileNameSplit[0];

        string stepchartPath = "Assets/Resources/Stepcharts/" + fileName + ".json";
        StreamReader reader = new StreamReader(stepchartPath);
        
        return JsonUtility.FromJson<Stepchart>(reader.ReadToEnd());
    }

    public bool IsSongLoaded
    {
        get { return _audioSource.clip != null; }
    }

    public bool IsPlaying
    {
        get { return _audioSource.isPlaying; }
    }

    public float SongLength
    {
        get { return IsSongLoaded ? _audioSource.clip.length : 0f; }
    }

    public float CurrentTrackBeatDelay
    {
        get { return 60000f / _stepchart.bpm; }
    }

    public float CurrentTrackBeat
    {
        get { return _audioSource.time / CurrentTrackBeatDelay; }
    }
 
    public Step[] Steps
    {
        get { return _stepchart.steps; }
    }

    public float BPM
    {
        get { return _stepchart.bpm; }
    }

    public float MaxHz
    {
        get { return _stepchart.maxHz; }
    }

    public float MinHz
    {
        get { return _stepchart.minHz; }
    }

    public Stepchart Chart
    {
        get { return _stepchart; }
    }
    
    public float Volume
    {
        get { return _audioSource.volume; }
        set { _audioSource.volume = value; }
    }


    // Editor only
    ////////////////////////////
    public void GoToBeat(int beat)
    {
        _audioSource.time = 60f / BPM * beat;
    }
}
