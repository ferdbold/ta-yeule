using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CurveScroller : MonoBehaviour
{
    // Members
    private CurveGenerator _curveGenerator;
    private MusicPlayer _musicPlayer;

    private bool _scroll;
    // The number of X units needed to scroll per second according to the current track BPM
    private float _scrollingSpeed;
    private float _initialX;

    void Awake()
    {
        _initialX = transform.position.x;

        _curveGenerator = GetComponent<CurveGenerator>();
        _musicPlayer = GameObject.FindWithTag("MusicPlayer").GetComponent<MusicPlayer>();

        MusicPlayer.OnSongLoaded += OnSongLoaded;
        MusicPlayer.OnSongStarted += OnSongStarted;
        MusicPlayer.OnSongPaused += OnSongPaused;
        MusicPlayer.OnSongStopped += OnSongStopped;
    }

    void OnDestroy()
    {
        MusicPlayer.OnSongLoaded -= OnSongLoaded;
        MusicPlayer.OnSongStarted -= OnSongStarted;
        MusicPlayer.OnSongPaused -= OnSongPaused;
        MusicPlayer.OnSongStopped -= OnSongStopped;
    }

    void Update()
    {
        if (_scroll)
        {
            Scroll();
        }
    }

    private void OnSongLoaded()
    {
        _scrollingSpeed = _curveGenerator._xUnitsPerBeat * _musicPlayer.BPM / 60f;
    }

    private void OnSongStarted()
    {
        _scroll = true;
    }

    private void OnSongPaused()
    {
        _scroll = false;
    }

    private void OnSongStopped()
    {
        _scroll = false;
        _scrollingSpeed = 0.0f;

        transform.position = new Vector3(_initialX, 0f, 0f);
    }

    private void Scroll()
    {
        transform.Translate(-_scrollingSpeed * Time.deltaTime, 0f, 0f);
    }

    // Editor only
    ///////////////////////////

    public void GoToBeat(int beat)
    {
        transform.position = new Vector3(_initialX - _curveGenerator._xUnitsPerBeat * beat, 0f, 0f);
    }
}
