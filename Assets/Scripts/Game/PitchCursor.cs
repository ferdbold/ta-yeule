using UnityEngine;

public class PitchCursor : MonoBehaviour
{
    public float TopBound;
    public float BottomBound;

    private MicStats micStats;
    private float maxHz = 400;
    private float minHz = 150;

    private float initialX;

    void Start()
    {
        initialX = transform.position.x;

        micStats = FindObjectOfType<MicStats>();
        MusicPlayer.OnSongLoaded += () => 
        { 
            MusicPlayer player = FindObjectOfType<MusicPlayer>();
            maxHz = player.MaxHz;
            minHz = player.MinHz;
        };
    }

    void Update()
    {
        float ypos;

        float ratio = ( micStats.GetPitch() - minHz ) / (maxHz - minHz);

        ypos = ( TopBound - BottomBound ) * ratio;

        ypos = Mathf.Clamp(ypos, BottomBound, TopBound);

        transform.position = new Vector3(initialX, ypos, 0.0f);
    }
}
