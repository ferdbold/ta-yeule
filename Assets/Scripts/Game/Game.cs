using System.Collections;
using UnityEditor;
using UnityEngine;
using UnityEngine.Events;

[CustomEditor(typeof(Game))]
public class GameEditor : Editor
{
    private int _beatValue = 0;
    
    public override void OnInspectorGUI()
    {
        DrawDefaultInspector();

        Game script = (Game)target;

        if (EditorApplication.isPlaying)
        {
            if (GUILayout.Button("Load Debug Song"))
            {
                script.LoadDebugSong();
            }

            if (script.IsSongLoaded)
            {
                if (!script.IsSongPlaying)
                {
                    if (GUILayout.Button("Play"))
                    {
                        script.StartCurrentSong();
                    }
                }
                else
                {
                    if (GUILayout.Button("Pause"))
                    {
                        script.PauseCurrentSong();
                    }
                }

                if (GUILayout.Button("Stop"))
                {
                    script.StopCurrentSong();        
                }

                if (GUILayout.Button("Raise voice"))
                {
                    script.ResetVoice();
                }
                
                GUILayout.Space(10);

                _beatValue = int.Parse(EditorGUILayout.TextField("Go to Beat:", _beatValue.ToString()));
                if (GUILayout.Button("Go to Beat"))
                {
                    script.GoToBeat(_beatValue);
                }
            }

            GUILayout.Space(10);

            if (GUILayout.Button("Add 10 health"))
            {
                script.ChangeHealth(10);
            }
            
            if (GUILayout.Button("Remove 10 health"))
            {
                script.ChangeHealth(-10);
            }
        }
    }
}

public class Game : MonoBehaviour
{
    // Props
    public AudioClip DebugSong;

    public float Health = 70f;
    public float MaxHealth = 100f;

    // Events
    public delegate void HealthAction(float health);
    public static event HealthAction OnHealthChanged;

    // Members
    private CurveScroller _curveScroller;
    private MusicPlayer _musicPlayer;
    private VoiceParser _voiceParser;
    private float score = 0;

    private bool Died = false;

    public void Awake()
    {
        _curveScroller = GameObject.FindWithTag("CurveContainer").GetComponentInChildren<CurveScroller>();
        _musicPlayer = GameObject.FindWithTag("MusicPlayer").GetComponent<MusicPlayer>();
        _voiceParser = FindObjectOfType<VoiceParser>();
        VoiceParser.OnRefreshTrigger += _musicPlayer.RefreshVoice;
    }

    private void FixedUpdate()
    {
        if(_musicPlayer.IsPlaying && !Died)
        {
            float tickScore = _voiceParser.OnPitchForBeat(_musicPlayer.CurrentTrackBeat, _musicPlayer.Chart);
            score += tickScore;

            if(tickScore >= 1)
            {
                ChangeHealth(0.1f);
            }
            else
            {
                ChangeHealth(-0.1f * tickScore);
            }

            float voiceDimDelay = Time.deltaTime / 20 ;
            _musicPlayer.LowerVoice(voiceDimDelay);
        }
    }

    public void LoadSong(AudioClip song)
    {
        _musicPlayer.Load(song);
    }

    public void StartCurrentSong()
    {
        _musicPlayer.Play();
        _musicPlayer.RefreshVoice();
    }

    public void PauseCurrentSong()
    {
        _musicPlayer.Pause();
    }

    public void StopCurrentSong()
    {
        _musicPlayer.Stop();
        _musicPlayer.RefreshVoice();
    }

    public void ChangeHealth(float value)
    {
        if(!Died)
        {
            float newValue = Health + value;

            if (newValue <= 0f && Health > 0)
            {
                StartCoroutine(GameOver());
                Died = true;
            }

            newValue = Mathf.Clamp(newValue, 0f, 100f);

            if (newValue != Health)
            {
                Health = newValue;
                OnHealthChanged(Health);
            }
        }
    }

    private IEnumerator GameOver()
    {
        while (_musicPlayer.Volume >= 0.01)
        {
            _musicPlayer.Volume = _musicPlayer.Volume - 0.01f;
            yield return new WaitForSeconds(0.1f);
        }
    }

    // Editor only
    ////////////////////
    public void LoadDebugSong()
    {
        LoadSong(DebugSong);
    }

    public bool IsSongLoaded
    {
        get { return _musicPlayer ? _musicPlayer.IsSongLoaded : false; }
    }

    public bool IsSongPlaying
    {
        get { return _musicPlayer ? _musicPlayer.IsPlaying : false; }
    }

    public void GoToBeat(int beat)
    {
        _musicPlayer.GoToBeat(beat);
        _curveScroller.GoToBeat(beat);
    }

    public void ResetVoice()
    {
        _musicPlayer.RefreshVoice();
    }
}
