using System;

[System.Serializable]
public class Step
{
    public float beat;

    // [Range]
    public float pitch;

    public float X
    {
        get { return beat; }
    }

    public float Y
    {
        get { return pitch; }
    }
}

[System.Serializable]
public class Stepchart
{
    public float bpm;
    public Step[] steps;
    public float minHz;
    public float maxHz;
}
