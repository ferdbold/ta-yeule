using System.Linq;
using UnityEngine;
using UnityEngine.Assertions;

public class VoiceParser : MonoBehaviour
{
    const float BEAT_THRESHOLD = 1;
    const float PITCH_THRESHOLD = 0.1f;
    const float BEAT_FALLOFF_LIMIT = 2;
    const float PITCH_FALLOFF_LIMIT = 0.2f;
    const float REFRESH_DELAY = 1f;
    const float REFRESH_PITCH_REQUIRED = 1000f;

    public delegate void ParserAction();
    public static event ParserAction OnRefreshTrigger;

    private MicStats MicStats;

    void Update()
    {
        //debug stuff
        // Stepchart stepchart = new Stepchart();
        // stepchart.steps = new Step[2];
        // stepchart.steps[0] = new Step();
        // stepchart.steps[0].beat = 1;
        // stepchart.steps[0].pitch = 0.5f;
        // stepchart.steps[1] = new Step();
        // stepchart.steps[1].beat = 2;
        // stepchart.steps[1].pitch = 0.5f;
        // stepchart.minHz = 100f;
        // stepchart.maxHz = 400f;
        //Debug.Log(OnPitchForBeat(1.5f, stepchart).ToString() + " points");

        //actual usefull stuff
        ManageRefresh();
    }

    // Start is called before the first frame update
    void Start()
    {
        MicStats = FindObjectOfType<MicStats>();
    }

    //range 0-1
    public float OnPitchForBeat( float beat, Stepchart chart )
    {
        Step minClosest = null;
        Step maxClosest = null;
        int currentIndex = 1; //start at 1 so we end at chart.steps last properly

        Step[] steps = chart.steps; //.Where(x => x.pitch >= 0).ToArray();

        while ( (minClosest == null || maxClosest == null) && currentIndex < steps.Length )
        {
            Step lower = steps[currentIndex - 1];
            if (minClosest == null)
            {
                minClosest = lower;

                if (minClosest.beat > beat)
                {
                    //edge for before the first beat
                    maxClosest = minClosest;
                    break;
                }
            }
            if (lower.beat < beat)
            {
                minClosest = lower;
            }

            Step higher = steps[currentIndex];
            if (higher.beat >= beat)
            {
                maxClosest = higher;
            }

            currentIndex++;
        }

        float beatRangeFactor = 1f;

        {
            float distFromMin = Mathf.Abs(minClosest.beat - beat);
            float distFromMax = Mathf.Abs(maxClosest.beat - beat);

            float minBeatDist = Mathf.Min(distFromMin, distFromMax);

            if (minBeatDist > BEAT_FALLOFF_LIMIT)
            {
                return 0;
            }
            else if (minBeatDist >= BEAT_THRESHOLD )
            {
                beatRangeFactor = ( minBeatDist - BEAT_THRESHOLD ) / (BEAT_FALLOFF_LIMIT - BEAT_THRESHOLD);
                Assert.IsTrue(beatRangeFactor >= 0 && beatRangeFactor <= 1);
            }
        }

        float pitchRangeFactor = 1f;
        float pitchToRange = ( MicStats.GetPitch() - chart.minHz ) / ( chart.maxHz - chart.minHz); //Range 0-1 can go above or below
        {
            float pitchExpectedAtBeat = LinearInterpol(beat, minClosest.X, maxClosest.X, minClosest.Y, maxClosest.Y);
            float distanceFromLineY = Mathf.Abs(pitchExpectedAtBeat - pitchToRange);

            if (distanceFromLineY > PITCH_FALLOFF_LIMIT)
            {
                return 0;
            }
            else if (distanceFromLineY >= PITCH_THRESHOLD)
            {
                pitchRangeFactor = ( distanceFromLineY - PITCH_THRESHOLD ) / (PITCH_FALLOFF_LIMIT - PITCH_THRESHOLD);
                Assert.IsTrue(pitchRangeFactor > 0 && pitchRangeFactor <= 1);
            }
        }

        return beatRangeFactor * pitchRangeFactor;
    }

    private float timeRefreshing;
    public void ManageRefresh()
    {
        if( MicStats.GetPitch() >= REFRESH_PITCH_REQUIRED && MicStats.GetDBValue() > -20f)
        {
            timeRefreshing += Time.deltaTime;
            if( timeRefreshing >= REFRESH_DELAY)
            {
                if (OnRefreshTrigger != null)
                    OnRefreshTrigger();
                timeRefreshing = 0f;
                Debug.Log( "Refreshed" );
            }
        }
        else
        {
            timeRefreshing = 0;
        }
    }

    static public float LinearInterpol(float x, float x0, float x1, float y0, float y1)
    {
        if ((x1 - x0) == 0)
        {
            return (y0 + y1) / 2;
        }
        return y0 + (x - x0) * (y1 - y0) / (x1 - x0);
    }
}
