using System.Linq;
using UnityEngine;

public class MicStats : MonoBehaviour
{
    int qSamples = 1024;  // array size
    float refValue = 0.1f; // RMS value for 0 dB
    float threshold = 0.02f;      // minimum amplitude to extract pitch

    float rmsValue;   // sound level - RMS
    float dbValue;    // sound level - dB
    float pitchValue; // sound pitch - Hz
    float lastTruePitch = float.MaxValue;

    private float[] samples; // audio samples
    private float[] spectrum; // audio spectrum
    private float fSample;
    private AudioSource audioSource;

    public float GetDBValue()
    {
        return dbValue;
    }

    public float GetPitch()
    {
        if (pitchValue <= 50f)
        {
            return lastTruePitch;
        }
        return pitchValue;
    }

    void Start()
    {
        samples = new float[qSamples];
        spectrum = new float[qSamples];
        fSample = AudioSettings.outputSampleRate;

        audioSource = GetComponent<AudioSource>();
        audioSource.clip = Microphone.Start(Microphone.devices.First(), true, 10, 44100);
        audioSource.Play();
    }

    void AnalyzeSound()
    {
        audioSource.GetOutputData(samples, 0); // fill array with samples
        float sum = 0;
        for (int i = 0; i < qSamples; i++)
        {
            sum += samples[i] * samples[i]; // sum squared samples
        }
        rmsValue = Mathf.Sqrt(sum / qSamples); // rms = square root of average
        dbValue = 20 * Mathf.Log10(rmsValue / refValue); // calculate dB
        if (dbValue < -160) dbValue = -160; // clamp it to -160dB min
                                        // get sound spectrum
        GetComponent<AudioSource>().GetSpectrumData(spectrum, 0, FFTWindow.BlackmanHarris);
        float maxV = 0;
        var maxN = 0;
        for (int i = 0; i < qSamples; i++)
        { // find max 
            if (!(spectrum[i] > maxV) || !(spectrum[i] > threshold))
                continue;

            maxV = spectrum[i];
            maxN = i; // maxN is the index of max
        }
        float freqN = maxN; // pass the index to a float variable
        if (maxN > 0 && maxN < qSamples - 1)
        { // interpolate index using neighbours
            var dL = spectrum[maxN - 1] / spectrum[maxN];
            var dR = spectrum[maxN + 1] / spectrum[maxN];
            freqN += 0.5f * (dR * dR - dL * dL);
        }
        if (pitchValue >= 50f)
        {
            lastTruePitch = pitchValue;
        }
        pitchValue = freqN * (fSample / 2) / qSamples;
    }

    // Update is called once per frame
    void Update()
    {
        AnalyzeSound();

        //Debug.Log("RMS: " + rmsValue.ToString("F2"));
        //Debug.Log(dbValue.ToString("F1") + " dB");
        //Debug.Log(GetPitch().ToString("F0") + " Hz");
    }
}
